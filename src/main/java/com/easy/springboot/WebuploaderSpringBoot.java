package com.easy.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@EnableAutoConfiguration
@ComponentScan("com.easy")
public class WebuploaderSpringBoot {
	public static void main(String[] args) {
		SpringApplication.run(WebuploaderSpringBoot.class, args);
	}

}
