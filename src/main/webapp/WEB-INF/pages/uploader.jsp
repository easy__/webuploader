<%@ page language="java" import="java.util.*" pageEncoding="utf-8"%>
<%
String path = request.getContextPath();
String basePath = request.getScheme()+"://"+request.getServerName()+":"+request.getServerPort()+path+"/";
%>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
  <head>
    <base href="<%=basePath%>">
    
    <title>My JSP 'MyJsp.jsp' starting page</title>
    
	<meta http-equiv="pragma" content="no-cache">
	<meta http-equiv="cache-control" content="no-cache">
	<meta http-equiv="expires" content="0">    
	<meta http-equiv="keywords" content="keyword1,keyword2,keyword3">
	<meta http-equiv="description" content="This is my page">
	<!--
	<link rel="stylesheet" type="text/css" href="styles.css">
	-->
<!--引入CSS-->
<link rel="stylesheet" type="text/css" href="/res/webuploader-0.1.5/webuploader.css"></link>

<!--引入JS-->
<script type="text/javascript" src="https://cdn.staticfile.org/jquery/3.2.1/jquery.js"></script>
<script type="text/javascript" src="/res/webuploader-0.1.5/webuploader.js"></script>
  
 <script type="text/javascript">
  //var projcetName =$("#project").val();
 $(function(){
	var $list = $('#thelist'),
       $btn = $('#ctlBtn');
	var state = 'pending';
	var fileFlag=false;
	var fileMd5;
	//自定义参数 文件名
	var fileName;
	var count=0;//当前正在上传的文件在数组中的下标，一次上传多个文件时使用  
   var filesArr=new Array();//文件数组：每当有文件被添加进队列的时候 就push到数组中  
   //监听分块上传过程中的三个时间点  
   WebUploader.Uploader.register({  
       "before-send-file":"beforeSendFile",  //整个文件上传前调用
       "before-send":"beforeSend",  //每个分片上传前
       "after-send-file":"afterSendFile",  //分片上传完毕
   },{  
       //时间点1：所有分块进行上传之前调用此函数  
       beforeSendFile:function(file){  
       	console.log("beforeSendFile");
           var deferred = WebUploader.Deferred();  
           //1、计算文件的唯一标记，用于断点续传  
           (new WebUploader.Uploader()).md5File(file,0,10*1024*1024)  
               .progress(function(percentage){  
                   $('#item1').find("p.state").text("正在读取文件信息...");  
               })  
               .then(function(val){  
                   fileMd5=val;  
                   $('#item1').find("p.state").text("成功获取文件信息...");
                   console.log("获取文件信息:"+file.name);
                 //秒传验证
               	$.ajax({  
                       type:"POST",  
                       url:"uploader/checkFileExists",  
                       data:{  
                           fileMd5:fileMd5,
                       },  
                       success:function(response){
                       	fileFlag=response.flag;
                       	var tempPath =response.path;
                       	if(fileFlag){
                       		console.log("跳过文件");
                       		uploader.skipFile(file);
                       		//deferred.reject();
                       		var path=$("#filePath").val();
                       		if(path!=""){
                       			path=path+"|"+tempPath;
                       		}else{
                       			path=tempPath;
                       		}
                       		$("#filePath").val(path);
                       		//
                       		count++;
                       	}
                       	//获取文件信息后进入下一步  
                           deferred.resolve();  
                       }
               	});
                   
                   
               });  
           return deferred.promise();  
       },  
       //时间点2：如果有分块上传，则每个分块上传之前调用此函数  
       beforeSend:function(block){
       	console.log("beforeSend");
           var deferred = WebUploader.Deferred();  
             
           $.ajax({  
               type:"POST",  
               url:"uploader/checkOrMerge?action=checkChunk",  
               data:{  
                   //文件唯一标记  
                   fileMd5:fileMd5,  
                   //当前分块下标  
                   chunk:block.chunk,  
                   //当前分块大小  
                   chunkSize:block.end-block.start  
               },  
               dataType:"json",  
               success:function(response){  
                   if(response.ifExist){ 
                   	console.log("分片存在:"+block.chunk)
                       //分块存在，跳过  
                       deferred.reject();  
                   }else{
                   	console.log("分片不存在:"+block.chunk)
                       //分块不存在或不完整，重新发送该分块内容  
                       deferred.resolve();  
                   }  
               }  
           });  
             
           this.owner.options.formData.fileMd5 = fileMd5;
           console.log("继续执行")
           //deferred.resolve();  
           return deferred.promise();  
       },  
       //时间点3：所有分块上传成功后调用此函数  
       afterSendFile:function(file){
       	console.log("afterSendFile");
       	fileName=file.name; //为自定义参数文件名赋值
       	if(!fileFlag){
           //如果分块上传成功，则通知后台合并分块  
           $.ajax({  
               type:"POST",  
               url:"uploader/checkOrMerge?action=mergeChunks",  
               data:{  
                   fileMd5:fileMd5,
                   fileName:fileName,
                   ext:file.ext, //文件扩展名
                   //projectName: $("#project option:selected").text() //项目名称
               },  
               success:function(data){
               	//var data= eval('(' + response + ')');
               	var filePath = data.path;
               	console.log(filePath);
               	//存储文件路径
               	var path=$("#filePath").val();
           		if(path!=""){
           			path=path+"|"+filePath;
           		}else{
           			path=filePath;
           		}
           		$("#filePath").val(path);
           		
           		 count++; //每上传完成一个文件 count+1
           		 //*****************************
           		 //注意 :每一个文件上传完成后都需要把count+1,因此在秒传时也需要+1
           		 //*****************************
           		 
                    if(count<=filesArr.length-1){  
                        uploader.upload(filesArr[count].id);//上传文件列表中的下一个文件  
                    }
               }  
           });
           fileFlag=false;
       }
       	$( '#'+file.id ).find('b.state').text('上传成功');
           $( '#'+file.id ).find('b.state').css("color","green");
       }  
   });  
	
	
   var uploader = WebUploader.create({
     resize: false, // 不压缩image     
     swf: '/res/webuploader-0.1.5/uploader.swf', // swf文件路径
     formData: { projectName: ""},
     server: 'uploader/upload', // 文件接收服务端。
     pick: '#picker', // 选择文件的按钮。可选
     chunked: true, //是否要分片处理大文件上传
     chunkSize:2*1024*1024, //分片上传，每片2M，默认是5M
     
     //auto: false //选择文件后是否自动上传
    chunkRetry : 2, //如果某个分片由于网络问题出错，允许自动重传次数
    threads:'5',        //同时运行5个线程传输
    fileNumLimit:'1000',  //文件总数量只能选择10个 
     //runtimeOrder: 'html5,flash',
     // 在上传当前文件时，准备好下一个文件
	  prepareNextFile:true,
	  duplicate : false,//是否重复上传（同时选择多个一样的文件），true可以重复上传  
      accept: {
        title: '语音上传',
        extensions: 'wav,rar',
        mimeTypes: 'audio/x-wav,.rar'//允许上传的文件类型
      }
   });
   // 当有文件被添加进队列的时候
   uploader.on( 'fileQueued', function( file ) {
       $list.append( '<div id="' + file.id + '" class="item">' +
           '<span class="info">' + file.name + '</span>' +
           '<b class="state" style="width:90px;">等待上传...</b>' +
       '</div>' );
   });
   //绑定uploadBeforeSend事件来给每个独立的文件添加参数
   uploader.on( 'uploadBeforeSend', function( block, data ) {
   	//设置data参数
   	//data.projectName= $("#project").find("option:selected").text();   // 将存在file对象中的md5数据携带发送过去。
	},2);
   // 文件上传过程中创建进度条实时显示。
   uploader.on( 'uploadProgress', function( file, percentage ) {
       var $li = $( '#'+file.id ),
           $percent = $li.find('.progress .progress-bar');

       // 避免重复创建
       if ( !$percent.length ) {
           $percent = $('<div class="progress progress-striped active">' +
             '<div class="progress-bar" role="progressbar" style="width: 0%">' +
             '</div>' +
           '</div>').appendTo( $li ).find('.progress-bar');
       }
		//进度条以百分比的形式显示
       $li.find('b.state').text('上传中'+Math.floor(percentage * 100) + '%' );

       $percent.css( 'width', percentage * 100 + '%' );
   });
   // 文件上传成功
   uploader.on( 'uploadSuccess', function( file,ret) {
   	console.log("uploadSuccess");
   	//返回文件的保存路径
   /* 	if(ret.flag==true){
   		console.log(ret.path);
   		var path=$("#filePath").val();
   		if(path!=""){
   			path=path+"|"+ret.path;
   		}else{
   			path=ret.path;
   		}
   		$("#filePath").val(path);
   	}*/
       // $( '#'+file.id ).find('b.state').text('上传成功');
       //$( '#'+file.id ).find('b.state').css("color","green");
   });

   // 文件上传失败，显示上传出错
   uploader.on( 'uploadError', function( file,ret ) {
   		status=false;
       $( '#'+file.id ).find('b.state').text('上传失败');
   });
   // 完成上传完
   uploader.on( 'uploadComplete', function( file ) {
       $( '#'+file.id ).find('.progress').fadeOut();
   });
   
   uploader.on('all', function(type){  
       if (type === 'startUpload'){  
           state = 'uploading';  
       }else if(type === 'stopUpload'){  
           state = 'paused';  
       }else if(type === 'uploadFinished'){  
           state = 'done';  
       }  
 
       if (state === 'uploading'){  
           $btn.text('暂停上传');  
       } else {  
           $btn.text('开始上传');  
       }  
   }); 
   $btn.on('click', function () {
           if ($(this).hasClass('disabled')) {
               return false;
           }
           if (state === 'uploading'){
               uploader.stop(true);  
           } else {  
               /* //当前上传文件的文件名  
               var currentFileName;  
               //当前上传文件的文件id  
               var currentFileId;  
               //count=0 说明没开始传 默认从文件列表的第一个开始传  
               console.log("count:"+count);
               if(count==0){  
                   currentFileName=filesArr[0].name;  
                   currentFileId=filesArr[0].id;  
               }else{  
                   if(count<=filesArr.length-1){  
                       currentFileName=filesArr[count].name;  
                       currentFileId=filesArr[count].id;  
                   }  
               }
               uploader.upload(currentFileId);  */
           	uploader.upload();
           }
           
           //uploader.upload();
           // if (state === 'ready') {
           //     uploader.upload();
           // } else if (state === 'paused') {
           //     uploader.upload();
           // } else if (state === 'uploading') {
           //     uploader.stop();
           // }
       });

});
 </script>
 
  
  </head>
  
  <body>
  <div class="demo">
  <input type="text" id="filePath" name="filePath"/>
            <h3>1、文件上传</h3>
            <div id="uploadfile">
                <!--用来存放文件信息-->
                <div id="thelist" class="uploader-list"></div>
                <div class="form-group form-inline">
                    <div id="picker" style="float:left">选择文件</div> &nbsp;
                    <button id="ctlBtn" class="btn btn-default" style="padding:8px 15px;">开始上传</button>
                </div>
            </div>
        </div>
  
  </body>
</html>
